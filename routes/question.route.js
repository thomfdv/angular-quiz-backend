const express = require('express');
const router = express.Router();
const questionController = require('../controllers/index.controller').questionController;

router.get('/', questionController.getAllQuestions);
router.get('/:id', questionController.getQuestionById);
router.get('/category/:id', questionController.getQuestionsByCategoryId);
router.post('/', questionController.createQuestion);
router.put('/:id', questionController.updateQuestion);
router.delete('/:id', questionController.deleteQuestion);

module.exports = router;
