const questionRoutes = require('./question.route');
const answerRoutes = require('./answer.route');
const categoryRoutes = require('./category.route');

module.exports = {
    questionRoutes,
    answerRoutes,
    categoryRoutes,
}
