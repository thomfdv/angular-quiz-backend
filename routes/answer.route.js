const express = require('express');
const router = express.Router();
const answerController = require('../controllers/index.controller').answerController;

router.get('/', answerController.getAllAnswers);
router.get('/:id', answerController.getAnswerById);
router.get('/question/:id', answerController.getAnswersByQuestionId);
router.post('/', answerController.createAnswer);
router.put('/:id', answerController.updateAnswer);
router.delete('/:id', answerController.deleteAnswer);

module.exports = router;
