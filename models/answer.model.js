const mongoose = require('mongoose');

const answerSchema = new mongoose.Schema({
    questionId: Number,
    answerLabel: String,
    isCorrect: Boolean,
});

module.exports = mongoose.model('Answer', answerSchema);
