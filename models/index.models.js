const questionModel = require('./question.model');
const categoryModel = require('./category.model');
const answerModel = require('./answer.model');
const userModel = require('./user.model');

module.exports = {
    questionModel,
    categoryModel,
    answerModel,
    userModel,
}
