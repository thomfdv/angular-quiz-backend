const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
    id: Number,
    categoryLabel: String,
});

module.exports = mongoose.model('Category', categorySchema);
