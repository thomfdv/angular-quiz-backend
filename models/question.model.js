const mongoose = require('mongoose');

const questionSchema = new mongoose.Schema({
    id: Number,
    questionLabel: String,
    categoryId: Number,
});

module.exports = mongoose.model('Question', questionSchema);
