const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config();

const app = express();
const port = process.env.PORT || 3000;

app.use(cors());
app.use(bodyParser.json());

// Connexion à MongoDB
mongoose.connect(`mongodb+srv://thomas:${process.env.MONGO_DB_PASSWORD}@cluster0.cisucu3.mongodb.net/?retryWrites=true&w=majority`)
    .then(() => console.log('Connected to MongoDB'))
    .catch(err => console.error('Could not connect to MongoDB', err));

// Définition des routes
app.get('/api', async (req, res) => {
    res.json({ message: 'Welcome to the Quiz API' });
});

const routes = require('./routes/index.route');

app.use('/api/questions', routes.questionRoutes);
app.use('/api/answers', routes.answerRoutes);
app.use('/api/categories', routes.categoryRoutes);

app.listen(port, () => {
    console.log(`Quiz API listening at http://localhost:${port}`);
});
