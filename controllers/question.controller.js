const Question = require('../models/index.models').questionModel;

exports.getAllQuestions = async (req, res) => {
    try {
        const questions = await Question.find();
        res.json(questions);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

exports.getQuestionById = async (req, res) => {
    try {
        const question = await Question.findById(req.params.id);
        if (!question) return res.status(404).send('Question not found');
        res.json(question);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

exports.getQuestionsByCategoryId = async (req, res) => {
    try {
        const questions = await Question.find({ categoryId: req.params.id });
        if (!questions) return res.status(404).send('Questions not found');
        res.json(questions);
    } catch (error) {
        res.status(500).send(error.message);
    }
}

exports.createQuestion = async (req, res) => {
    try {
        const newQuestion = new Question(req.body);
        await newQuestion.save();
        res.status(201).json(newQuestion);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

exports.updateQuestion = async (req, res) => {
    try {
        const updatedQuestion = await Question.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!updatedQuestion) return res.status(404).send('Question not found');
        res.json(updatedQuestion);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

exports.deleteQuestion = async (req, res) => {
    try {
        const question = await Question.findByIdAndDelete(req.params.id);
        if (!question) return res.status(404).send('Question not found');
        res.status(204).send();
    } catch (error) {
        res.status(500).send(error.message);
    }
};
