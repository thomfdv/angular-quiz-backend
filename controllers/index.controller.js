const questionController = require('../controllers/question.controller');
const answerController = require('../controllers/answer.controller');
const categoryController = require('../controllers/category.controller');

module.exports = {
    questionController,
    answerController,
    categoryController,
}
