const Answer = require('../models/index.models').answerModel;

// Récupérer toutes les réponses
exports.getAllAnswers = async (req, res) => {
    try {
        const answers = await Answer.find();
        res.json(answers);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

// Récupérer une réponse par ID
exports.getAnswerById = async (req, res) => {
    try {
        const answer = await Answer.findById(req.params.id);
        if (!answer) return res.status(404).send('Answer not found');
        res.json(answer);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

exports.getAnswersByQuestionId = async (req, res) => {
    try {
        const answers = await Answer.find({ questionId: req.params.id });
        if (!answers) return res.status(404).send('Answers not found');
        res.json(answers);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

// Créer une nouvelle réponse
exports.createAnswer = async (req, res) => {
    try {
        const newAnswer = new Answer(req.body);
        await newAnswer.save();
        res.status(201).json(newAnswer);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

// Mettre à jour une réponse
exports.updateAnswer = async (req, res) => {
    try {
        const updatedAnswer = await Answer.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!updatedAnswer) return res.status(404).send('Answer not found');
        res.json(updatedAnswer);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

// Supprimer une réponse
exports.deleteAnswer = async (req, res) => {
    try {
        const answer = await Answer.findByIdAndDelete(req.params.id);
        if (!answer) return res.status(404).send('Answer not found');
        res.status(204).send();
    } catch (error) {
        res.status(500).send(error.message);
    }
};
