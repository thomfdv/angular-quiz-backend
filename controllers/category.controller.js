const Category = require('../models/category.model');

// Récupérer toutes les catégories
exports.getAllCategories = async (req, res) => {
    try {
        const categories = await Category.find();
        res.json(categories);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

// Récupérer une catégorie par ID
exports.getCategoryById = async (req, res) => {
    try {
        const category = await Category.findOne({ id: req.params.id });
        if (!category) return res.status(404).send('Category not found');
        res.json(category);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

// Créer une nouvelle catégorie
exports.createCategory = async (req, res) => {
    try {
        const newCategory = new Category(req.body);
        await newCategory.save();
        res.status(201).json(newCategory);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

// Mettre à jour une catégorie
exports.updateCategory = async (req, res) => {
    try {
        const updatedCategory = await Category.findOneAndUpdate({ id: req.params.id }, req.body, { new: true });
        if (!updatedCategory) return res.status(404).send('Category not found');
        res.json(updatedCategory);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

// Supprimer une catégorie
exports.deleteCategory = async (req, res) => {
    try {
        const category = await Category.findOneAndDelete({ id: req.params.id });
        if (!category) return res.status(404).send('Category not found');
        res.status(204).send();
    } catch (error) {
        res.status(500).send(error.message);
    }
};
