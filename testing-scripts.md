# Testing Scripts

### Testing the API

```shell
curl http://localhost:3001/api
curl http://localhost:3001/api/questions
curl -X POST http://localhost:3001/api/questions -H "Content-Type: application/json" -d '{"id": 1, "questionLabel": "What is a component in Angular?", "categoryId":1}'
```

### Creating the beginner of the database

##### Create the categories

```shell
curl -X POST http://localhost:3001/api/categories \
-H "Content-Type: application/json" \
-d '{"id": 1, "categoryLabel": "Angular"}'
```

##### Create the questions

```shell
curl -X POST http://localhost:3001/api/questions \
-H "Content-Type: application/json" \
-d '{"id": 1, "questionLabel": "What is a component in Angular?", "categoryId": "1"}'
```

##### Create the answers

```shell
curl -X POST http://localhost:3001/api/answers \
-H "Content-Type: application/json" \
-d '{"questionId": 1, "answerLabel": "A reusable building block for the user interface", "isCorrect": true}'

curl -X POST http://localhost:3001/api/answers \
-H "Content-Type: application/json" \
-d '{"questionId": 1, "answerLabel": "A style in Angular", "isCorrect": false}'

curl -X POST http://localhost:3001/api/answers \
-H "Content-Type: application/json" \
-d '{"questionId": 1, "answerLabel": "A service for handling HTTP requests", "isCorrect": false}'

curl -X POST http://localhost:3001/api/answers \
-H "Content-Type: application/json" \
-d '{"questionId": 1, "answerLabel": "A routing configuration", "isCorrect": false}'
```
